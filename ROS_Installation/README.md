INTRODUCTION
------------

This resporitory will teach new Ottabotics members the basics of ROS. Content will include how to download ROS on a user's machine and a basic tutorial on how to use the framework. 

REQUIREMENTS
------------

This module requires the following:
	- To be a Developer in Learning Project
	- Set up SSH key
	- Understand basics of programming and how to use python 
	- Basic Arduino skills
	- Understand C/C++ 
	- Passionate about Robotics :)

INSTALLATION
------------

If you have created the Learning Project repository on your machine, navigate to "ROS_Tutorial"

MAINTAINERS
-----------

Current mainater:
	*Yanushka Gnanamuttu - ygnan057@uottawa.ca